import React,{useState} from 'react';
import Board from './Board';
import "./Game.css"
import {calculateWinner} from "../helper"

const Game = () => {
    const [board,setBoard]=useState(Array(9).fill(null))
    const [xIsNext,setXIsNext]=useState(true)
    const winner = calculateWinner(board)

    const habdleClick = (index)=>{
        const boardCopy = [...board]
        // Определить был ли клик по ячейке или игра закончена
        if(winner || boardCopy[index])return
        // Определить чей ход Х ? О
        boardCopy[index]=xIsNext?"X":"O"
        // Обновить наш стейт
        setBoard(boardCopy)
        setXIsNext(!xIsNext)
    }

    const startNewGame =()=>{
        return(
            <button className="start_btn" onClick={()=>setBoard(Array(9).fill(null))} >Очистить поле</button>
        )
    }
    return (
        <div className="wrapper">
            {startNewGame()}
            <Board squares={board} click={habdleClick}/>
            <p className="game_info">
                {winner ? "Победитель " + winner: "Сейчас ходит" +(xIsNext? "  Х" : "  О" )}
            </p>
        </div>
    );
}

export default Game;
